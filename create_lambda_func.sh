check_arg_exit(){
	# オプションの引数がなかったら終了
	if [ -z "$OPTARG" ]; then
		echo "${0##*/}: option needs arg -- $opt"
		exit 1
	fi
}

REGION=ap-northeast-1
FUNC_NAME=lambda_error_log_to_slack
ROLE_NAME=lambda_error_log_to_slack_exec_role
TIME_ZONE=Asia/Tokyo
SLACK_WEBHOOK_URL=https://hooks.slack.com/services/xxxxx

while getopts ":-:" opt; do
	if [ $opt = "-" ]; then
		opt=`echo ${OPTARG} | awk -F'=' '{print $1}'`
		OPTARG=`echo ${OPTARG} | awk -F'=' '{print $2}'`
	fi
 
    case "$opt" in
    	region)
        	check_arg_exit
    		REGION=${OPTARG}
            ;;
        role-name)
        	check_arg_exit
            ROLE_NAME=${OPTARG}
            ;;
        function-name)
        	check_arg_exit
            FUNC_NAME=${OPTARG}
            ;;
        timezone)
        	check_arg_exit
            TIME_ZONE=${OPTARG}
            ;;
        slack-webhook-url)
        	check_arg_exit
            SLACK_WEBHOOK_URL=${OPTARG}
            ;;
		* )
			echo "invalid option -- $opt"
			exit 1
			;; # 未定義のlong optionのエラーはここで出力
    esac
    
done

# ロールのARNを取得する
ROLE_ARN=`./get_role.sh --role-name=$ROLE_NAME`

# ファイルを全てZipにまとめる
zip -r ./_log_alert.zip ./* > /dev/null

echo "aws lambda create-function \
 --region $REGION \
 --zip-file fileb://./_log_alert.zip \
 --role $ROLE_ARN \
 --handler index.handler \
 --runtime nodejs4.3 \
 --function-name $FUNC_NAME \
 --environment Variables={TZ=$TIME_ZONE} \
 --environment {\"Variables\":{\"TZ\":\"$TIME_ZONE\",\"SLACK_WEBHOOK_URL\":\"$SLACK_WEBHOOK_URL\"}}"

# Lambda関数を作成する
aws lambda create-function \
 --region $REGION \
 --zip-file fileb://./_log_alert.zip \
 --role $ROLE_ARN \
 --handler index.handler \
 --runtime nodejs4.3 \
 --function-name $FUNC_NAME \
 --environment "{\"Variables\":{\"TZ\":\"$TIME_ZONE\",\"SLACK_WEBHOOK_URL\":\"$SLACK_WEBHOOK_URL\"}}"
 
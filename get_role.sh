get_role() {
	ROLE_ARN=`aws iam get-role --role-name $1 | jq -r ".Role.Arn"`
	echo $ROLE_ARN
}

check_arg_exit(){
	# オプションの引数がなかったら終了
	if [ -z "$OPTARG" ]; then
		echo "${0##*/}: option needs arg -- $opt"
		exit 1
	fi
}

while getopts "r:-:" opt; do
	if [ $opt = "-" ]; then
		opt=`echo ${OPTARG} | awk -F'=' '{print $1}'`
		OPTARG=`echo ${OPTARG} | awk -F'=' '{print $2}'`
	fi
 
    case "$opt" in
        r | role-name)
        	check_arg_exit
            get_role ${OPTARG} 
            ;;
		* )
			echo "invalid option -- $opt"
			exit 1
			;; # 未定義のlong optionのエラーはここで出力
    esac
    
done

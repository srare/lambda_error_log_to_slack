check_arg_exit(){
	# オプションの引数がなかったら終了
	if [ -z "$OPTARG" ]; then
		echo "${0##*/}: option needs arg -- $opt"
		exit 1
	fi
}

FUNC_NAME=dev_translator

while getopts ":-:" opt; do
	if [ $opt = "-" ]; then
		opt=`echo ${OPTARG} | awk -F'=' '{print $1}'`
		OPTARG=`echo ${OPTARG} | awk -F'=' '{print $2}'`
	fi
 
    case "$opt" in
        function-name)
        	check_arg_exit
            FUNC_NAME=${OPTARG}
            ;;
		* )
			echo "invalid option -- $opt"
			exit 1
			;; # 未定義のlong optionのエラーはここで出力
    esac
    
done

# ファイルを全てZipにまとめる
zip -r ./_log_alert.zip ./* > /dev/null

# Lambda関数を作成する
aws lambda update-function-code \
 --zip-file fileb://./_log_alert.zip \
 --function-name $FUNC_NAME
 

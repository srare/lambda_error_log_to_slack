'use strict'; 

const Promise = require('bluebird');
const request = require('request-promise');
const zlib = Promise.promisifyAll(require('zlib'));
const slack_url = process.env.SLACK_WEBHOOK_URL;
const region = process.env.REGION || process.env.AWS_REGION; // AWS_REGIONは予約済み環境変数

exports.handler = function(event, context, callback) {
  Promise.coroutine(function*(){
    var payload = new Buffer(event.awslogs.data, 'base64');
    let resultData = yield zlib.gunzipAsync(payload);
    var result = resultData.toString('utf8');
    result = result.replace("--ignore-ssl-errors", "--ignore-ssl-e_rrors");
    
    var skip = true;
    if (result.toLowerCase().indexOf("error") != -1) {
      skip = false;
    }
    if (result.indexOf("Task timed out") != -1) {
      skip = false;
    }
    if (skip) {
      return;
    }
    let resultJson = JSON.parse(result);
    
    let date = new Date(resultJson.logEvents[0].timestamp);
    let datetime = `${date.getFullYear()}/${date.getMonth()+1}/${date.getDate()} ${date.toTimeString()}`;
    let function_name = resultJson.logGroup.split("/").pop();
    let group = resultJson.logGroup;
    let stream = resultJson.logStream;
    let log_url = `https://${region}.console.aws.amazon.com/cloudwatch/home?region=${region}#logEventViewer:group=${group};stream=${stream}`;
    var log_messages = [];
    var request_id = null;
    var main_error = "";
    for (var i=0; i<resultJson.logEvents.length; i++) {
      if (resultJson.logEvents[i].message.toLowerCase().indexOf("error") != -1 || resultJson.logEvents[i].message.indexOf("Task timed out") != -1) {
        request_id = requestIdFromMessage(resultJson.logEvents[i].message);
        main_error = stringByRemoveDatetimeRequestId(resultJson.logEvents[i].message);
        break;
      }
    }
    for (var i=0; i<resultJson.logEvents.length; i++) {
      if (resultJson.logEvents[i].message.indexOf("RequestId: ") != -1) {
        continue;
      }
      if (request_id != requestIdFromMessage(resultJson.logEvents[i].message)) {
        continue;
      }
      log_messages.push(stringByRemoveDatetimeRequestId(resultJson.logEvents[i].message));
    }
    let message = `:warning:${function_name}\n${main_error}\n\n:clock2:${datetime}\n:page_facing_up:${log_url};filter="${encodeURIComponent(request_id)}"\n\n>>>${log_messages.join("\n")}`;
    console.log("message : " + message);
    yield request({
      method: 'POST',
      uri: slack_url,
      headers: {
          "Content-Type" : 'application/json',
      },
      body: JSON.stringify({
          text : message,
      }),
    });
    callback(null, "success");
  })().catch((e) => {
    if (e) {
      callback(e);
    }
  });
};

const requestIdFromMessage = function (message) {
  let mat = message.match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z\s+([a-z0-9\-]+)\s+/);
  if (mat == null) {
    return null;
  }
  return mat[1];
};

const stringByRemoveDatetimeRequestId = function (message) {
  return message.replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z\s+([a-z0-9\-]+)\s+/, "");
};

create_role() {
	aws iam create-role --role-name $1 --assume-role-policy-document file://role_policy.json
}

check_arg_exit(){
	# オプションの引数がなかったら終了
	if [ -z "$OPTARG" ]; then
		echo "${0##*/}: option needs arg -- $opt"
		exit 1
	fi
}

while getopts "r:-:" opt; do
	if [ $opt = "-" ]; then
		opt=`echo ${OPTARG} | awk -F'=' '{print $1}'`
		OPTARG=`echo ${OPTARG} | awk -F'=' '{print $2}'`
	fi
 
    case "$opt" in
        r | role-name)
        	check_arg_exit
            create_role ${OPTARG} 
            ;;
		* )
			echo "invalid option -- $opt"
			exit 1
			;; # 未定義のlong optionのエラーはここで出力
    esac
    
done
